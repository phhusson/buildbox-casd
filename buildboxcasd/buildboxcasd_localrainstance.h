/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_LOCALRAINSTANCE_H
#define INCLUDED_BUILDBOXCASD_LOCALRAINSTANCE_H

#include <buildboxcasd_fslocalassetstorage.h>
#include <buildboxcasd_rainstance.h>

#include <memory>

using namespace build::bazel::remote::asset::v1;

namespace buildboxcasd {

class LocalRaInstance
    /* This class implements the logic for methods that service a RA server,
     * as defined by the Remote Asset API.
     */
    : public RaInstance {
  public:
    LocalRaInstance(std::shared_ptr<FsLocalAssetStorage> asset_storage);

    grpc::Status FetchBlob(const FetchBlobRequest &request,
                           FetchBlobResponse *response) override;

    grpc::Status FetchDirectory(const FetchDirectoryRequest &request,
                                FetchDirectoryResponse *response) override;

    grpc::Status PushBlob(const PushBlobRequest &request,
                          PushBlobResponse *response) override;

    grpc::Status PushDirectory(const PushDirectoryRequest &request,
                               PushDirectoryResponse *response) override;

  private:
    std::shared_ptr<FsLocalAssetStorage> d_assetStorage;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_LOCALRAINSTANCE_H
