/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_executionproxyinstance.h>

using namespace buildboxcasd;
using namespace buildboxcommon;

ExecutionProxyInstance::ExecutionProxyInstance(
    const buildboxcommon::ConnectionOptions &execution_endpoint,
    const std::string &instance_name)
    : ExecutionInstance(instance_name),
      d_grpc_client(std::make_shared<GrpcClient>()),
      d_remote_instance_name(execution_endpoint.d_instanceName),
      d_stop_requested(false)
{
    d_grpc_client->init(execution_endpoint);
    d_exec_client =
        std::make_shared<RemoteExecutionClient>(d_grpc_client, nullptr);
    d_exec_client->init();
}

grpc::Status
ExecutionProxyInstance::Execute(ServerContext *ctx,
                                const ExecuteRequest &request,
                                ServerWriterInterface<Operation> *writer)
{
    std::function<bool()> stop_lambda = [&] {
        return d_stop_requested.load() || ctx->IsCancelled();
    };
    try {
        ExecuteRequest remote_request;
        remote_request.CopyFrom(request);
        remote_request.set_instance_name(d_remote_instance_name);
        std::string operation_prefix = d_instance_name + "#";
        return d_exec_client->proxyExecuteRequest(remote_request, stop_lambda,
                                                  writer, operation_prefix);
    }
    catch (GrpcError &e) {
        return e.status;
    }
}

grpc::Status
ExecutionProxyInstance::WaitExecution(ServerContext *ctx,
                                      const WaitExecutionRequest &request,
                                      ServerWriterInterface<Operation> *writer)
{
    std::function<bool()> stop_lambda = [&] {
        return d_stop_requested.load() || ctx->IsCancelled();
    };
    try {
        WaitExecutionRequest remote_request;
        remote_request.CopyFrom(request);
        remote_request.set_name(rewriteOperationName(request.name()));
        std::string operation_prefix = d_instance_name + "#";
        return d_exec_client->proxyWaitExecutionRequest(
            remote_request, stop_lambda, writer, operation_prefix);
    }
    catch (GrpcError &e) {
        return e.status;
    }
}

grpc::Status
ExecutionProxyInstance::GetOperation(const GetOperationRequest &request,
                                     Operation *response)
{
    try {
        GetOperationRequest remote_request;
        remote_request.CopyFrom(request);
        remote_request.set_name(rewriteOperationName(request.name()));
        grpc::Status status =
            d_exec_client->proxyGetOperationRequest(remote_request, response);
        response->set_name(d_instance_name + "#" + response->name());
        return status;
    }
    catch (GrpcError &e) {
        return e.status;
    }
}

grpc::Status
ExecutionProxyInstance::ListOperations(const ListOperationsRequest &request,
                                       ListOperationsResponse *response)
{
    try {
        ListOperationsRequest remote_request;
        remote_request.CopyFrom(request);
        remote_request.set_name(d_remote_instance_name);
        grpc::Status status = d_exec_client->proxyListOperationsRequest(
            remote_request, response);

        // Rewrite operation names to include the local instance name.
        for (Operation &operation : *response->mutable_operations()) {
            operation.set_name(d_instance_name + "#" + operation.name());
        }

        return status;
    }
    catch (GrpcError &e) {
        return e.status;
    }
}

grpc::Status
ExecutionProxyInstance::CancelOperation(const CancelOperationRequest &request,
                                        google::protobuf::Empty *response)
{
    try {
        CancelOperationRequest remote_request;
        remote_request.CopyFrom(request);
        remote_request.set_name(rewriteOperationName(request.name()));
        return d_exec_client->proxyCancelOperationRequest(remote_request,
                                                          response);
    }
    catch (GrpcError &e) {
        return e.status;
    }
}

void ExecutionProxyInstance::stop() { d_stop_requested = true; }

std::string ExecutionProxyInstance::rewriteOperationName(std::string name)
{
    size_t idx = name.find_first_of('#');
    if (idx == std::string::npos) {
        return name;
    }
    return name.substr(idx + 1);
}
